from FirebaseManager import FirebaseManager
from Detail import RadioDetail
import cPickle as pickle


def updateDetail(detail):
    type = raw_input("Type of detail   (  Optional(_)  )")
    nominal = raw_input("Write nominal of radioDetail: E3 E6 E9 E12 E24  (  Optional(_)  )")
    amountOnScheme = raw_input("Anount on Scheme  (  Optional(_)  )")
    reserved = raw_input("Mark of similar detail  (  Optional(_)  )")

    notEmpty = lambda value: value if value else None
    notEmptyNominal = lambda value: value if value in ["E3", "E6", "E9", "E12", "E24"] else "E0"
    detail.setType(notEmpty(type)) \
        .setAmountOnScheme(notEmpty(amountOnScheme)) \
        .setReservedDetail(notEmpty(reserved)) \
        .setNominal(notEmptyNominal(nominal))

    return detail

def nameSort(det):
    return det.getMark()

def sortByAmountOnScheme(det):
    return det.getAmountOnScheme()

def sortDetail(details, by):
    dets = sorted(details, key=eval(by))
    return dets

def createNewDetail():
    detail = RadioDetail()
    mark = ""
    #required atribute
    while True:
        mark = raw_input("Set mark for new RadioDetail ")

        if str(mark):
            print(mark)
            detail.setMark(mark)
            break

    #Optionals atributes
    updateDetail(detail)

    return detail

def comparedDetail(first_detail, action,  second_detail):
    if eval("first_detail {} second_detail".format(action)):
        print("\t>>>The FIRST detail is {} SECOND detail".format(action))
    else:
        print("\t>>>The FIRST detail is not {} SECOND detail".format(action))



if __name__ == '__main__':
    manager = FirebaseManager()

    state = -1
    while True:
        print("Menu:")
        print("\t1. Create new radioDetail ")
        print("\t2. Delete radioDetail")
        print("\t3. Update old value")
        print("\t4. Compare radioDetails")
        print("\t5. Search ")
        print("\t6. Output all Details")
        print("\t7. Read from file (*.pkl)")
        print("\t8. Save to file from DB")
        print("\n\t0. Exit\n")

        while True:
            state = int(input("Make your choice  "))
            print(state)
            if state in range(0, 9):
                break

        if state == 0:
            break
        elif state == 1:
            detail = createNewDetail()
            manager.saveRadioDetail(detail.convertIntoList())
        elif state == 2:
            mark = raw_input("Write mark of detail you want to delete  ")
            manager.deleteDocument(manager.searchDetail(unicode(mark)))
        elif state == 3:
            mark = raw_input("Write mark of detail you want to update  ")
            id_doc = manager.searchDetail(unicode(mark))
            detail = RadioDetail.from_dict(manager.getDocumentDict(id_doc))
            detail = updateDetail(detail)
            manager.updateDoc(id_doc, detail)
        elif state == 4:
            mark = raw_input("Write mark of FIRST detail you want to compare  ")
            id_first_doc = manager.searchDetail(unicode(mark))
            first_detail = RadioDetail.from_dict(manager.getDocumentDict(id_first_doc))
            first_detail.output()

            mark = raw_input("Write mark of SECOND detail you want to compare   ")
            id_first_doc = manager.searchDetail(unicode(mark))
            second_detail = RadioDetail.from_dict(manager.getDocumentDict(id_first_doc))
            second_detail.output()

            sign = raw_input("Type your Comparing Sign( <,  >,  == )  ")

            comparedDetail(first_detail, sign, second_detail)

        elif state == 5:
            category = raw_input(">>>Write category you want to search( mark, type, nominal, amountOnScheme):  ")

            keyWord = raw_input(">>>Type your KeyWord in {} you want search".format(category))
            details_id = manager.searchDetailsFor(category, keyWord)

            for detail_id in details_id:
                detail = RadioDetail.from_dict(manager.getDocumentDict(detail_id))
                detail.output()
                print("")

        elif state == 6:
            sort = int(raw_input("Sort (0 - No | 1 - yes)"))
            docs = manager.getAllRadioDetail()
            details = []
            for doc in docs:
                detail = RadioDetail.from_dict(doc)
                details.append(detail)

            if sort == 1:
                type = raw_input("CHOOSE THESE:  <nameSort> or <sortByAmountOnScheme>")

                if type == "sortByAmountOnScheme":
                    sorted_detail = sortDetail(details, "sortByAmountOnScheme")
                    print("\t\t<< All RadioDetails >>")
                    for detail in sorted_detail:
                        print "\n"
                        detail.output()
                else:
                    sorted_detail = sortDetail(details, "nameSort")
                    print("\t\t<< All RadioDetails >>")
                    for detail in sorted_detail:
                        print "\n"
                        detail.output()
            else:
                print("\t\t<< All RadioDetails >>")
                for detail in details:
                    print "\n"
                    detail.output()
        elif state == 7:
            path = raw_input("Type path to file *.pkl")
            print "Reading file..."
            save = int(raw_input("Save to DB (0 - No | 1 - Yes)"))
            for detail in RadioDetail.loadFrom(path):
                print detail.getMark()
                if save == 1:
                    manager.saveRadioDetail(detail.convertIntoList())

        elif state == 8:
            docs = manager.getAllRadioDetail()
            path = raw_input("Type filename *.pkl")
            print("\t\t<< All RadioDetails >>")
            details = []
            for doc in docs:
                detail = RadioDetail.from_dict(doc)
                details.append(detail)
            RadioDetail.save_object(details, path)


        print("\t <  ---    ---  >")