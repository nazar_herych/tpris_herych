

from abc import ABCMeta, abstractmethod
import six

@six.add_metaclass(ABCMeta)
class Nominal:

    @abstractmethod
    def value(self):
        pass

class E0(Nominal):
    def value(self):
        return 0.0

class E3(Nominal):
    def value(self):
        return 3.0

class E6(Nominal):
    def value(self):
        return 6.0

class E9(Nominal):
    def value(self):
        return 9.0

class E12(Nominal):
    def value(self):
        return 12.0

class E24(Nominal):
    def value(self):
        return 24.0


class NominalFactory(object):
    def make_resistance(self, object_type):
        return eval(object_type)()
