
import firebase_admin
from firebase_admin import credentials, firestore
import uuid

class FirebaseManager(object):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(FirebaseManager, cls).__new__(cls)
            cls.__cred = credentials.Certificate("./serviceAccountKey.json")
            firebase_admin.initialize_app(cls.__cred)

            cls.db = firestore.client()
        return cls.instance

    def saveRadioDetail(self, list):
        #get path
        radiodetail_reference = self.db.collection(u'RadioDetails')
        detailUID = str(uuid.uuid4())

        #create new id
        detail_reference = radiodetail_reference.document(detailUID)

        #set RadioDetail obj
        detail_reference.set(list)

    def getAllRadioDetail(self):
        docs = self.db.collection(u'RadioDetails').get()
        dict = {}
        for doc in docs:
            dict[doc.id] = doc.to_dict()

        return dict.values()

    def searchDetail(self, mark):
        docs = self.db.collection(u'RadioDetails').where(u'mark', u'==', mark).get()

        for doc in docs:
            return doc.id

    def searchDetailsFor(self, label, value):
        docs = self.db.collection(u'RadioDetails').where(unicode(label), u'>=', unicode(value)).get()
        ids = []
        for doc in docs:
            ids.append(doc.id)

        return ids

    def getDocumentDict(self, id):
        return self.db.collection(u'RadioDetails').document(id).get().to_dict()

    def deleteDocument(self, id):
        self.db.collection(u'RadioDetails').document(id).delete()

    def updateDoc(self, id, document):
        # get path
        radiodetail_reference = self.db.collection(u'RadioDetails').document(id)

        # update RadioDetail obj
        radiodetail_reference.set(document.convertIntoList())


