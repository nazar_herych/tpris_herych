from abc import ABCMeta, abstractmethod
import six
from Nominal import NominalFactory
import cPickle as pickle


def setAll(self, mark=None, type=None, nominal=None, amount=None, reservedDetail=None):
    self.setMark(mark)
    self.setType(type)
    self.setNominal(nominal)
    self.setAmountOnScheme(amount)
    self.setReservedDetail(reservedDetail)

    return self



@six.add_metaclass(ABCMeta)
class Detail:

    def __init__(self):
        self.mark = None
        self.type = None
        self.nominal = None
        self.amountOnScheme = None
        self.reservedDetailMark = None


    @abstractmethod
    def createDetail(self ):
        pass


    @staticmethod
    def from_dict(dict):
        radioDetail = RadioDetail()

        for key, value in dict.iteritems():
            if str(key) == "nominal":
                radioDetail.setNominal("E"+str(int(round(float(value)))))
            else:
                keyStr = str(key)[0].capitalize() + str(key[1: len(str(key))])
                eval("radioDetail.set" + keyStr)(value)
        return radioDetail

    @staticmethod
    def loadFrom(filename):
        with open(filename, "rb") as f:
            while True:
                try:
                    yield pickle.load(f)
                except EOFError:
                    break

    def setMark(self, naming):
        self.mark = naming
        return self

    def setType(self, type):
        self.type = type
        return self

    def setNominal(self, nominalStr="E0"):
        nominal_factory = NominalFactory()
        self.nominal = nominal_factory.make_resistance(nominalStr)
        return self

    def setAmountOnScheme(self, amount):
        self.amountOnScheme = amount
        return self

    def setReservedDetail(self, mark):
        self.reservedDetailMark = mark
        return self




    def getMark(self):
        return self.mark

    def getType(self):
        return self.type

    def getNominal(self):
        return self.nominal

    def getAmountOnScheme(self):
        return self.amountOnScheme

    def getReservedDetail(self):
        return self.reservedDetailMark


    def handleIntoList(self, key, value):
        if value is None:
            return {}
        else:
            return {key: unicode(value)}


    def convertIntoList(self):
        list = {}


        list.update(self.handleIntoList(u'mark', self.getMark()))
        list.update(self.handleIntoList(u'type', self.getType()))
        if not self.getNominal():
            list.update(self.handleIntoList(u'nominal', self.getNominal().value()))
        list.update(self.handleIntoList(u'amountOnScheme', self.getAmountOnScheme()))
        list.update(self.handleIntoList(u'reservedDetail', self.getReservedDetail()))

        return list


    def output(self):
        nominal = self.getNominal()
        strNominal = ""
        if nominal is None:
            strNominal = ""
        else:
            strNominal = str(self.getNominal().value())
        print("\t[MARK:  {} \n\tType:  {}\n\tNominal: {}\n\tAmountOnScheme: {}\n\tReservedDetail:  {}]"\
              .format(str(self.getMark()), str(self.getType()),
                      str(self.getAmountOnScheme()),
                      strNominal,
                      str(self.getReservedDetail())))



class RadioDetail(Detail):

    def __del__(self):
        # print "Deliting object..."
        self.mark = None
        self.type = None
        self.nominal = None
        self.amountOnScheme = None
        self.reservedDetailMark = None

    def createDetail(self):
        self.setNominal("E0")

    def __lt__(self, other):
        if other.nominal is None:
            return False
        else:
            if self.nominal is None:
                return True
            return (self.nominal.value() < other.nominal.value())



    def __gt__(self, other):
        return (self.nominal.value() > other.nominal.value())

    def __eq__(self, other):
        if (self.nominal.value() == other.nominal.value()):
            return (self.getAmountOnScheme() == other.getAmountOnScheme())
        else:
            return False

    def __le__(self, other):
        if self == other:
            return True

        if self.getNominal().value() == other.getNominal().value():
            return self.getAmountOnScheme() <= other.getAmountOnScheme()

    def __ge__(self, other):
        if self == other:
            return True

        if self.getNominal().value() == other.getNominal().value():
            return self.getAmountOnScheme() <= other.getAmountOnScheme()

    @staticmethod
    def save_object(details, filename):
        with open(filename, 'wb') as output:  # Overwrites any existing file.
            for detail in details:
                pickle.dump(detail, output, pickle.HIGHEST_PROTOCOL)

