# import google.cloude
import firebase_admin
from firebase_admin import credentials, firestore

class FirebaseManager(object):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(FirebaseManager, cls).__new__(cls)
            cls.__cred = credentials.Certificate("./serviceAccountKey.json")
            firebase_admin.initialize_app(cls.__cred)

            cls.db = firestore.client()
        return cls.instance



    def saveToDatabase(self, dict):
        queue_ref = self.db.collection(u'PriorityQueue')

        for key, value in dict.items():
            queue_ref.document(key).set(value)



    def loadQueue(self):
        priority_queue = self.db.collection(u'PriorityQueue')
        docs = priority_queue.get()
        dict = {}
        for doc in docs:
            dict[doc.id] = doc.to_dict()
            priority_queue.document(doc.id).delete()
        return dict
