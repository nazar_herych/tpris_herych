# [START app]
import logging

from flask import Flask
from flask import request
import json
from FirebaseManager import FirebaseManager
from flask import jsonify

app = Flask(__name__)
manager = FirebaseManager()

@app.route('/')
def root():
    """Return a friendly HTTP greeting."""
    return 'Hello World!'


# @app.errorhandler(500)
# def server_error(e):
#     logging.exception('An error occurred during a request.')
#     return """
#     An internal error occurred: <pre>{}</pre>
#     See logs for full stacktrace.
#     """.format(e), 500


@app.route('/hello/<name>')
def hello(name):
    return 'Hello user ' + str(name)


@app.route('/LoadQueue')
def summary():
    data = jsonify(manager.loadQueue())
    return data

@app.route('/SaveQueue', methods=['POST'])
def parse_request():
    print (request.is_json)
    content = request.get_json()
    print (content)
    dict = json.loads(content)
    print('Dict   ' + str(dict))
    manager.saveToDatabase(dict)
    return 'JSON posted'



if __name__ == '__main__':
    # This is used when running locally. Gunicorn is used to run the
    # application on Google App Engine. See entrypoint in app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
# [END app]
