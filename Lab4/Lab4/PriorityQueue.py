
class PriorityQueue:
    def __init__(self):
        self.size = 0
        self.list = []

    def add(self, key, value):
        tup = (key, value)
        index = self.getIndexFor(key)
        self.list.insert(index, tup)

    def remove(self):
        # print(self.list)
        return self.list.pop()


    def getIndexFor(self, priority):
        if self.isEmpty():
            return 0

        for item in self.list:
            if priority >= item[0]:
                return self.list.index(item)
        return len(self.list)

    def peek(self):
        if self.isEmpty():
            return None
        else:
            return self.highestPriority()

    def highestPriority(self):
        highest = 0
        if self.isEmpty():
            return None
        else:
            for i in self.list:
                if i[0] > highest:
                    highest = i[0]
        return highest

    def lowerPriority(self):
        low = 10000000
        if self.isEmpty():
            return None
        else:
            for i in self.list:
                if i[0] < low:
                    low = i[0]
        return low

    def sizeOfQueue(self):
        return len(self.list)

    def isEmpty(self):
        if len(self.list) == 0:
            return True
        else:
            return False

    def output(self):
        for key, value in self.list:
            print("{} -> {}".format(key, value))

    def empty(self):
        while not self.isEmpty():
            self.remove()

    def to_dict(self):
        dict = {}
        prev_key = None
        counter = 0
        category = {}
        for item in reversed(self.list):
            if item[0] == prev_key:
                category[unicode(counter)] = unicode(item[1])
            else:
                #Save previos category
                if prev_key is not None:
                    dict[unicode(prev_key)] = category

                #Reset data
                category = {}
                counter = 0
                prev_key = item[0]

                #Set new value
                category[unicode(counter)] = unicode(item[1])

            counter += 1

        dict[prev_key] = category
        return dict



    @staticmethod
    def from_dict(dict):
        queue = PriorityQueue()
        for key, value in dict.items():

            keylist = value.keys()
            keylist.sort()
            for key_inner in keylist:
                queue.add(int(key), unicode(value[key_inner],
                                            ).encode('utf-8'))
        return queue


