from PriorityQueue import *
import requests
import json

# from FirebaseManager import FirebaseManager

# manager = FirebaseManager()
queue = PriorityQueue()




if __name__ == '__main__':
    print("\tMENU:")
    print("1. Add data to queue")
    print("2. Remove from queue")
    print("3. Size of queue")
    print("4. Print All data in queue")
    print("5. Save data to Firebase")
    print("6. Retrieve data from Firebase")
    print("\n0. Exit\n")


    while True:
        choice = int(input("\nDo your decision:  "))

        if choice == 0:
            break
        elif choice == 1:
            priority = int(input("Type your Priority:  "))
            data = raw_input("Type your Data:  ")

            queue.add(priority, data)
        elif choice == 2:
            data = queue.remove()
            print("Removed Data  =>  {}".format(data))
        elif choice == 3:
            print queue.sizeOfQueue()
        elif choice == 4:
            queue.output()
        elif choice == 5:
            print("\t... saving to Firebase")
            jsObj = json.dumps(queue.to_dict(), ensure_ascii=False)
            print(jsObj)
            requests.post(url='https://lab4-205014.appspot.com/SaveQueue', json=jsObj)
            print("Saved!")

            #Empty queue
            queue.empty()

        elif choice == 6:
            print("\t... loading from Firebase")
            response = requests.get(url='https://lab4-205014.appspot.com/LoadQueue')
            print(response.content)
            queue = PriorityQueue.from_dict(json.loads(response.content))
            print("Loaded")
