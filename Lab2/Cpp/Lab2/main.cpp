#include <iostream>
#include "TextFile.h"
#include <time.h>

using namespace::std;

int main() {
    //start time in entery programm
    clock_t timer;
    timer = clock();

    string path = "/home/nazar/Documents/CLionProject/Lab2/File";
    TextFile * file = new TextFile(path);
    file->openFile();

    // Output str from file
    string str = *file->getText();

    cout << "\n\t<<<  Text from file  >>>" << endl;
    cout << str << endl;

    // Reverse str and output
    file->reverse();
    string reversed = *file->getText();

    cout << "\n\t<<<  Reversed text from file  >>>" << endl;
    cout << reversed << endl;

    file->saveToFile(*file->getText());

    //end time
    timer = clock() - timer;

    cout << "\n\nRequests was handled: [" << ((float)timer)/CLOCKS_PER_SEC << "] seconds."<<endl;

    return 0;
}