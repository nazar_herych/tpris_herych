//
// Created by nazar on 10.05.18.
//

#include "TextFile.h"
#include "iostream"
#include <fstream>



TextFile::TextFile(string filePath){
    this->filePath = filePath;
}

TextFile::~TextFile(){
    //dtor
}

string* TextFile::getText(){
    return &this->text;
}


void TextFile::setFileName(string naming){
    this->filePath = naming;
}


void TextFile::openFile(){
    ifstream fin(this->filePath); // open for reading
	string line, exeption;
	this->text = "";

	try {
		if (!fin.is_open())
		{
			throw ("Read: file hasn't been found!");
		}

		//read from file
		while (!fin.eof())
		{
			getline(fin, line);
			this->text += line;
			this->text += '\n';
		}
		fin.close();

		//Check if file is not empty
		this->isFileEmpty(text);

		cout << "Text was been readed!" << endl;
	}
	catch (char* msg){
		cout << msg << " in readfromFile()" << endl;
	}
}

void TextFile::saveToFile(string text){
    try	{
		isFileEmpty(text);

		ofstream out(this->filePath);
		out << text;
		out.close();

		cout << "Text's been saved to PATH = " << this->filePath << endl;
	}
	catch (char* exeption)	{
		cout << exeption << " while saving to file" << endl;
	}
}

void TextFile::isFileEmpty(string text) {
	if(text == ""){
		throw "File is empty";
	}else if(text.length() >= 1e+8){
		throw "Text is so big...";
	}
}

void TextFile::reverse() {
	TextFile::Reverse(*this->getText());
}