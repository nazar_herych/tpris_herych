//
// Created by nazar on 10.05.18.
//
#include "iostream"
#include <fstream>

#ifndef LAB2_TEXTFILE_H
#define LAB2_TEXTFILE_H


using namespace::std;

class TextFile
{
    public:
        TextFile(string);
        virtual ~TextFile();

        //reverse text
        static void Reverse(string& text){
            int n = text.length();

            // Swap character starting from two
            // corners
            for (int i=0; i<n/2; i++)
                swap(text[i], text[n-i-1]);
        };

        void reverse();

        //Process file
        void setFileName(string);
        void openFile();
        void saveToFile(string);

        //Get text of opened file
        string* getText();

    protected:

    private:
    string text;
    string filePath;

    void isFileEmpty(string text);
};


#endif //LAB2_TEXTFILE_H


