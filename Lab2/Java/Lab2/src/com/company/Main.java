package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        var path = "/home/nazar/IdeaProjects/Lab2/File";

        long start = System.currentTimeMillis();

        TextFile file = new TextFile(path);
        file.openFile();

        //Output str
        String str = file.getText();
        System.out.println("\n\t<<<  Text from file  >>>\n" + str);

        file.reverse();
        String reversedStr = file.getText();
        System.out.println("\n\t<<<  Reversed from file  >>>\n" + reversedStr);


        file.saveToFile(file.getText());

        long time = System.currentTimeMillis() - start;
        System.out.println("Requests was handling: [" + time + "] 10^(-3) seconds.");
    }
}