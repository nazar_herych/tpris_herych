package com.company;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;


public class TextFile {
    private String text;
    private String filePath;


    TextFile(String path){
        this.filePath = path;
    }

    static String Reverse(String text){
        int i, len = text.length();
        StringBuilder dest = new StringBuilder(len);

        for (i = (len - 1); i >= 0; i--){
            dest.append(text.charAt(i));
        }

        return dest.toString();
    };

    public void reverse(){
        String reversedText = TextFile.Reverse(this.text);
        this.text = reversedText;
    }

    //Process file
    void setFileName(String name){
        this.filePath = name;
    }

    void openFile(){
        try{
            String fileText = new String(Files.readAllBytes(Paths.get(this.filePath)), "windows-1251");
            this.text = fileText;
        }catch (NoSuchFileException ex){
            System.out.println("\t#" + "Check PATH to your file!");
        } catch (IOException exc){
            System.out.println("\t#" + exc.getMessage());
        }
    }

    void saveToFile(String text){


        try{
            this.isTextEmpty(text);
            FileWriter fw = new FileWriter(this.filePath);
            fw.write(text);

            System.out.println("\n\t>>>" + "Text's SAVED!");

            fw.close();
        } catch(IOException e){
            System.out.println("\t#" + e.getMessage());
        } catch (Exception ex){
            System.out.println("\t#" + ex.getMessage());
        }
    }


    private void isTextEmpty(String text) throws Exception {
        if(text == null || "".equals(text))
            throw new Exception("Text is empty");
        else if(text.length() >= 1e+8)
            throw new Exception("Text is so big...");
    }


    //Get text of opened file
    public String getText(){
        return this.text;
    }
}
