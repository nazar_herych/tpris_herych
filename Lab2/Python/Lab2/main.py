import time
from TextFile import TextFile

# Track Starting time
start_time = time.clock()

#Create new fiel obj
textFile = TextFile()

#Set Path to file
textFile.setFileName("File")
textFile.openFile()

#Get text of opened file
str = textFile.getStr()
print("\n\t<<<  Text from file  >>>")
print(str)

#Reverse text
reversed_str = TextFile.Reverse(str)

print("\n\t<<<  Reversed text from file  >>>")
print(reversed_str)


#Save reversed text to file
textFile.saveToFile(reversed_str)

#Output Time of durration operation
print "\nRequests was handling: [",time.clock() - start_time, "]seconds"