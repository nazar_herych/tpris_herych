
class TextFile:
    def __init__(self):
        self.file_name = None
        self.file = None
        self.string = None

    @staticmethod
    def Reverse(str):
        if not str:
            print(" --- This string is empty! ---")
            return None

        return str[::-1]

    def setFileName(self, name):
        self.file_name = name

    def getStr(self):
        if self.string is None:
            self.string = self.file.read()
        return self.string

    def openFile(self):
        if self.file_name is not None:
            try:
                print("Opening file [{}]".format(self.file_name))
                self.file = open('{}'.format(self.file_name), 'r+')
            except OSError as err:
                print("OS error: {0}".format(err))
            else:
                self.string = self.file.read()

                if len(self.string) > 1e8:
                    print("Can't read the file! Text is so big..." )
                    self.file = None


        else:
            print("Incorrect file name")


    def saveToFile(self, str):
        if self.file is not None:
            # self.file.
            self.file.write("\n\n")
            self.file.writelines(str)
        else:
            print("File is not opened")

    def __del__(self):
        self.file.close()