
var db = firebase.firestore();

document.getElementById("1").style.display="none";  
document.getElementById("2").style.display="none";
document.getElementById("3").style.display="none";
let rootNode = document.getElementById("root");

let evaluate = () => {
    let expr =  document.getElementById("expr").value;

    
    document.getElementById("result1").innerHTML=eval(expr);
}
document.getElementById("btn1").onclick=evaluate;
 function task() {
    document.getElementById("1").style.display="block";
    document.getElementById("1").onclick=evaluate;
    document.getElementById("2").style.display="none";
    document.getElementById("3").style.display="none";

 }
 function task2() {

    document.getElementById("1").style.display="none";  
    document.getElementById("2").style.display="block";
    document.getElementById("3").style.display="none";
 }
  function task3() {

    document.getElementById("1").style.display="none";  
    document.getElementById("3").style.display="block";
    document.getElementById("2").style.display="none";
 }
  function getMatrix(){
    var elementList = document.querySelectorAll("#matrix div");
    var matrix = [];
    for(var i=0;i<elementList.length;i++){
        matrix.push([]);
    }
    for(var i=0;i<elementList.length;i++){
        
        for(var j=0;j<elementList[i].childNodes.length;j++){
                matrix[i][j]= +elementList[i].childNodes[j].value ;
        }
    }
    return matrix;
 }
 function getMatrix1(){
    var elementList = document.querySelectorAll("#matrix1 div");
    var matrix = [];
    for(var i=0;i<elementList.length;i++){
        matrix.push([]);
    }
    for(var i=0;i<elementList.length;i++){
        
        for(var j=0;j<elementList[i].childNodes.length;j++){
                matrix[i][j]= +elementList[i].childNodes[j].value ;
        }
    }
    return matrix;
 }
  function getMatrix2(){
    var elementList = document.querySelectorAll("#matrix2 div");
    var matrix = [];
    for(var i=0;i<elementList.length;i++){
        matrix.push([]);
    }
    for(var i=0;i<elementList.length;i++){      
        for(var j=0;j<elementList[i].childNodes.length;j++){
                matrix[i][j]= +elementList[i].childNodes[j].value;
        }
    }
    return matrix;

 }
  function showMatrix() {
    
        matrixContainer = document.getElementById("matrixContainer");
        matrixContainer.innerHTML="";
        let matrix1= document.createElement("div");
        matrix1.id="matrix1";
        let matrix2= document.createElement("div");
        matrix2.id="matrix2";
        var size1= +document.getElementById("size1").value;
        var size2= +document.getElementById("size2").value;
        console.log(size1 +"\n" + size2);
        var arr=new Array(size1);
        arr.fill(new Array(size2));
        console.log(arr);
        for(var i=0; i < size1; i++){
            let row= document.createElement("div")
            for(var j=0; j < size2; j++){
                let input= document.createElement("input")
                input.value="0";
                row.appendChild(input)

            }
            matrix1.appendChild(row);
            

        }
        for(var i=0; i < size1; i++){
            let row= document.createElement("div")
            for(var j=0; j < size2; j++){
                let input= document.createElement("input");
                input.value="0";
                row.appendChild(input);

            }
            matrix2.appendChild(row);
            

        }
        matrixContainer.appendChild(matrix1);
        matrixContainer.appendChild(matrix2);
        let buttonsContainer = document.createElement("div");
        let plus = document.createElement("button");
            plus.id="plus";
            plus.innerHTML="+";
        let minus = document.createElement("button");
        minus.id="minus";
        minus.innerHTML="-";
        let mul = document.createElement("button");
        mul.id="mul";
        mul.innerHTML="*";
        let div = document.createElement("button");
        div.id="div";
        div.innerHTML="/";
        matrixContainer.appendChild(plus);
        matrixContainer.appendChild(minus);
        matrixContainer.appendChild(mul);
        matrixContainer.appendChild(div);
        matrixContainer.appendChild(buttonsContainer);
        document.getElementById("plus").onclick=function(){

            if(document.getElementById("resdiv")){
                document.getElementById("resdiv").remove();
            }
    
            var matrix1=getMatrix1();
            var matrix2=getMatrix2(); 
            var matrix3=getMatrix1();           
            for(let i=0;i<matrix2.length;i++){
                for(let j=0;j<matrix2[i].length;j++){
                    matrix3[i][j]=matrix1[i][j] + matrix2[i][j];
                }
            }
            var matrixRes=document.createElement("div");
            matrixRes.innerHTML=`Результат`;
            let matrix3div= document.createElement("div");
            matrix3div.id="matrix3";
            var size1= +document.getElementById("size1").value;
            var size2= +document.getElementById("size2").value;
            for(var i=0; i < size1; i++){
                let row= document.createElement("div")
                for(var j=0; j < size2; j++){
                    let input= document.createElement("input")
                    input.value=matrix3[i][j];
                    row.appendChild(input)

                }
                matrix3div.appendChild(row);
                

            }
            let resdiv= document.createElement("div")
            resdiv.appendChild(matrixRes);
             resdiv.appendChild(matrix3div);
             resdiv.id="resdiv";
             document.getElementById("2").appendChild(resdiv);

             var a= matrixToObject(matrix3);
             db.collection("Matrixes").add({ matrix:a             
             })
             .then(function(docRef) {
                 console.log("Document written with ID: ", docRef.id);
             })
             .catch(function(error) {
                 console.error("Error adding document: ", error);
             });

        };
        document.getElementById("minus").onclick=function(){
            if(document.getElementById("resdiv")){
                document.getElementById("resdiv").remove();
            }

            var matrix1=getMatrix1();
            var matrix2=getMatrix2(); 
            var matrix3=getMatrix1();           
            for(let i=0;i<matrix2.length;i++){
                for(let j=0;j<matrix2[i].length;j++){
                    matrix3[i][j]=matrix1[i][j] - matrix2[i][j];
                }
            }
            var matrixRes=document.createElement("div");
            matrixRes.innerHTML=`Результат`;
            let matrix3div= document.createElement("div");
            matrix3div.id="matrix3";
            var size1= +document.getElementById("size1").value;
            var size2= +document.getElementById("size2").value;
            for(var i=0; i < size1; i++){
                let row= document.createElement("div")
                for(var j=0; j < size2; j++){
                    let input= document.createElement("input")
                    input.value=matrix3[i][j];
                    row.appendChild(input)

                }
                matrix3div.appendChild(row);
                

            }
            let resdiv= document.createElement("div")
            resdiv.appendChild(matrixRes);
             resdiv.appendChild(matrix3div);
             resdiv.id="resdiv";
             document.getElementById("2").appendChild(resdiv);

             var a= matrixToObject(matrix3);
             db.collection("Matrixes").add({ matrix:a             
             })
             .then(function(docRef) {
                 console.log("Document written with ID: ", docRef.id);
             })
             .catch(function(error) {
                 console.error("Error adding document: ", error);
             });

        };
        document.getElementById("mul").onclick=function(){
            if(document.getElementById("resdiv")){
                document.getElementById("resdiv").remove();
            }
            var matrix1=getMatrix1();
            var matrix2=getMatrix2(); 
            var matrix3=getMatrix1();           
            for(let i=0;i<matrix2.length;i++){
                for(let j=0;j<matrix2[i].length;j++){
                    matrix3[i][j]=matrix1[i][j] * matrix2[i][j];
                }
            }
            var matrixRes=document.createElement("div");
            matrixRes.innerHTML=`Результат`;
            let matrix3div= document.createElement("div");
            matrix3div.id="matrix3";
            var size1= +document.getElementById("size1").value;
            var size2= +document.getElementById("size2").value;
            for(var i=0; i < size1; i++){
                let row= document.createElement("div")
                for(var j=0; j < size2; j++){
                    let input= document.createElement("input")
                    input.value=matrix3[i][j];
                    row.appendChild(input)

                }
                matrix3div.appendChild(row);
                

            }
            let resdiv= document.createElement("div")
            resdiv.appendChild(matrixRes);
             resdiv.appendChild(matrix3div);
             resdiv.id="resdiv";
             document.getElementById("2").appendChild(resdiv);

             var a= matrixToObject(matrix3);
             db.collection("Matrixes").add({ matrix:a             
             })
             .then(function(docRef) {
                 console.log("Document written with ID: ", docRef.id);
             })
             .catch(function(error) {
                 console.error("Error adding document: ", error);
             });

        };
        document.getElementById("div").onclick=function(){
            if(document.getElementById("resdiv")){
                document.getElementById("resdiv").remove();
            }
            var matrix1=getMatrix1();
            var matrix2=getMatrix2(); 
            var matrix3=getMatrix1();           
            for(let i=0;i<matrix2.length;i++){
                for(let j=0;j<matrix2[i].length;j++){
                    matrix3[i][j]=matrix1[i][j] / matrix2[i][j];
                }
            }
            var matrixRes=document.createElement("div");
            matrixRes.innerHTML=`Результат`;
            let matrix3div= document.createElement("div");
            matrix3div.id="matrix3";
            var size1= +document.getElementById("size1").value;
            var size2= +document.getElementById("size2").value;
            for(var i=0; i < size1; i++){
                let row= document.createElement("div")
                for(var j=0; j < size2; j++){
                    let input= document.createElement("input")
                    input.value=matrix3[i][j];
                    row.appendChild(input)

                }
                matrix3div.appendChild(row);
                

            }
            let resdiv= document.createElement("div")
            resdiv.appendChild(matrixRes);
             resdiv.appendChild(matrix3div);
             resdiv.id="resdiv";
             document.getElementById("2").appendChild(resdiv);

             var a= matrixToObject(matrix3);
             db.collection("Matrixes").add({ matrix:a             
             })
             .then(function(docRef) {
                 console.log("Document written with ID: ", docRef.id);
             })
             .catch(function(error) {
                 console.error("Error adding document: ", error);
             });
 

        };

    
    
 }
 function sortMatrix(){

    matrixContainer = document.getElementById("matrixContainer");
    matrixContainer.innerHTML="";
    let matrixDiv= document.createElement("div");
    matrixDiv.id="matrix";
    var size= +document.getElementById("size").value;
    var arr=new Array(size1);
    arr.fill(new Array(size2));
    for(var i=0; i < size; i++){
        let row= document.createElement("div")
        for(var j=0; j < size; j++){
            let input= document.createElement("input")
            input.value="1";
            row.appendChild(input)

        }
        matrixDiv.appendChild(row);
        

    }   
    matrixContainer.appendChild(matrixDiv);
    document.getElementById("3").appendChild(matrixContainer)
    let button = document.createElement("button");
        button.id="sort";
        button.innerHTML="sort";    
    matrixContainer.appendChild(button);
    document.getElementById("sort").onclick=function(){

        if(document.getElementById("resdiv")){
            document.getElementById("resdiv").remove();
        }   
        var matrix=getMatrix();
        var sortedMatrix=getMatrix();
        console.log(matrix);



        for(let i=0;i<matrix.length;i++){
            let a =Math.floor(matrix.length/2).toFixed();
            console.log(a);
            for(let j=0;j<matrix.legnth;j++){
            //  matrix3[i][j]=matrix1[i][j] + matrix2[i][j];
                if(matrix[i][j]===0){
                    for ( let k = j; k > 0; k -- )
                     {
                         matrix[i][k] = matrix[i][k-1];
                         matrix[i][k-1] = 0;
                     }    


                }
            }
            for(let j=a;j<matrix[i].length;j++){
            //  matrix3[i][j]=matrix1[i][j] + matrix2[i][j];
                if(matrix[i][j]===0){
                    let temp = matrix[i][j];
                    console.log(matrix[i]);
                    matrix[i].push(0);console.log(matrix[i]);
                    matrix[i].splice(j,1);console.log(matrix[i]);
                    
                }
            }

        }
        console.log(matrix);

        var matrixRes=document.createElement("div");
        matrixRes.innerHTML=`Результат`;
        let matrixSorted= document.createElement("div");
        matrixSorted.id="matrixSorted";
 



        for(var i=0; i < size; i++){
            let row= document.createElement("div")
            for(var j=0; j < size; j++){
                let input= document.createElement("input")
                input.value=matrix[i][j];
                row.appendChild(input)

            }
            matrixSorted.appendChild(row);          

        }
        let resdiv= document.createElement("div")
        
         resdiv.appendChild(matrixSorted);
         resdiv.id="resdiv";
         matrixContainer.appendChild(resdiv);
         var a= matrixToObject(matrix);
         db.collection("Matrixes").add({ matrix:a             
         })
         .then(function(docRef) {
             console.log("Document written with ID: ", docRef.id);
         })
         .catch(function(error) {
             console.error("Error adding document: ", error);
         });     

    };
 }

 function matrixToObject(matrix) {
    var obj = {};
    for(var i =0;i<matrix.length;i++){
        obj[i]={}
    }
    for(var i =0;i<matrix.length;i++){
        for(var j =0;j<matrix.length;j++){
            obj[i][j]=matrix[i][j];
        }
    }
    console.log(obj);
    return obj;

 }
