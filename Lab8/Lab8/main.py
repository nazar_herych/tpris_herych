from GraphWithThreads import Graph
from Queue import *



def menu():
    print("\tMenu")
    print("1. Generate graph ")
    print("2. Add edge to graph")
    print("3. Breadth-first search")
    print "4. Output graph"
    print("0. Exit")

    graph = Graph()

    while True:
        choise = int(input("\n>Your choise: "))

        if choise == 1:
            size = int(raw_input(">Enter amount of Edges"))

            graph.generateArray(size)
        if choise == 2:
            from_edges = int(raw_input(">Type From Edges(index) "))
            to_edges = int(raw_input(">Type To Edges(index) "))

            graph.addEdge(from_edges, to_edges)
            graph.addEdge(to_edges, from_edges)
        if choise == 3:
            start = int(raw_input(">Type index of start Edges: "))

            queue_of_edges = graph.BFS(start)
            queue_of_edges.printQueue()
        if choise == 4:
            print "Output:"
            graph.output()
            graph.outputMatrix()

        if choise == 0:
            break




if __name__=="__main__":
    menu()