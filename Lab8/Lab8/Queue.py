class Queue(object):
    def __init__(self, max_size=5000):
        self.size = max_size
        self.arr = []

    def enqueue(self, item):
        if self.is_full():
            print "Queue is full."
        else:
            self.arr.append(item)

    def dequeue(self):
        if self.is_empty():
            print "Queue is empty."
        else:
            self.arr.pop(0)

    def is_empty(self):
        return len(self.arr) == 0

    def is_full(self):
        return len(self.arr) == self.size

    def front(self):
        if self.is_empty():
            print "Queue is empty."
        else:
            print self.arr[0]

    def rear(self):
        if self.is_empty():
            print "Queue is empty."
        else:
            print self.arr[len(self.arr) - 1]

    def printQueue(self):
        print("{}".format(self.arr))