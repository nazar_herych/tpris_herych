from collections import defaultdict
from Queue import Queue
from random import randint

from OpenGL.GL import *
from OpenGL.arrays import vbo


# This class represents a directed graph
# using adjacency list representation
class Graph:

    # Constructor
    def __init__(self):
        # default dictionary to store graph
        # self.graph = {}
        self.graph = defaultdict(list)
        self.matrix = []

    # function to add an edge to graph
    def addEdge(self, u, v):
        self.graph[u].append(v)
        # self.graph[u] = v

    # Fill matrix with random value
    def generateArray(self, size):

        # self.graph = [None for x in range(size)]
        self.matrix = [[GLshort(0) for x in range(size)] for y in range(size)]

        for i in xrange(0, size):
            for j in xrange(0, size):
                # Predict self-reference
                if i == j:
                    continue

                if self.matrix[i][j].value == 0:

                    if randint(0, 1):
                        self.matrix[i][j] = GLshort(randint(0, 1))
                        self.matrix[j][i] = self.matrix[i][j]

        for i in xrange(0, size):
            for j in xrange(0, size):
                if self.matrix[i][j].value == 1:
                     self.addEdge(i, j)

    def output(self):
        for key, value in self.graph.iteritems():
            print("{} -> {}".format(key, value))

    def outputMatrix(self):
        print "\n\tMatrix"
        for raw in range(len(self.matrix)):
            str_raw = ""
            for col in self.matrix[raw]:
                str_raw += str(col.value) + " "
            print str_raw
            # print raw

    # Function to print a BFS of graph
    def BFS(self, s):

        # Mark all the vertices as not visited
        visited = [False] * (len(self.graph))

        # Create a queue for BFS
        queue = []

        # Result Queue
        result = Queue()

        # Mark the source node as
        # visited and enqueue it
        queue.append(s)
        visited[s] = True

        while queue:
            # Dequeue a vertex from
            # queue and print it
            s = queue.pop(0)
            result.enqueue(s)
            # print(str(s) + " ")

            # Get all adjacent vertices of the
            # dequeued vertex s. If a adjacent
            # has not been visited, then mark it
            # visited and enqueue it
            for i in self.graph[s]:
                if visited[i] == False:
                    queue.append(i)
                    visited[i] = True

        # result.printQueue()
        return result


