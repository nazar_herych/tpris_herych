from collections import defaultdict
from Queue import Queue
from random import randint

from OpenGL.GL import *
from OpenGL.arrays import vbo
import threading

threadLock = threading.Lock()


class Graph:

    class myThread(threading.Thread):
        def __init__(self, counter, size, action):
            threading.Thread.__init__(self)
            self.counter = counter
            self.size = size
            self.action = action

        def run(self):
            print "Starting " + self.name
            # Get lock to synchronize threads

            threadLock.acquire()
            self.action(self.counter, self.size)

            # Free lock to release next thread
            threadLock.release()

    # Constructor
    def __init__(self):
        # default dictionary to store graph
        # self.graph = {}
        self.graph = defaultdict(list)
        self.matrix = []


    def generate(self, counter, size):
        if counter == 1:

            self.createMatrix(size)
        else:
            self.fillGraph(size)

    def createMatrix(self, size):
        print("Creating matrix")

        self.matrix = [[GLshort(0) for x in range(size)] for y in range(size)]

        for i in xrange(0, size):
            for j in xrange(0, size):
                # Predict self-reference
                if i == j:
                    continue

                if self.matrix[i][j].value == 0:

                    if randint(0, 1):
                        self.matrix[i][j] = GLshort(randint(0, 1))
                        self.matrix[j][i] = self.matrix[i][j]

    def fillGraph(self, size):
        print("Fill dict")

        for i in xrange(0, size):
            for j in xrange(0, size):
                if self.matrix[i][j].value == 1:
                     self.addEdge(i, j)


    # function to add an edge to graph
    def addEdge(self, u, v):
        self.graph[u].append(v)
        # self.graph[u] = v


    # Fill matrix with random value

    def generateArray(self, size):
        thread1 = self.myThread(1, size, self.generate)
        thread2 = self.myThread(2,  size, self.generate)

        # Start new Threads
        thread1.start()
        thread2.start()


        thread1.join()
        thread2.join()


    # Function to print a BFS of graph
    def BFS(self, s):

        # Mark all the vertices as not visited
        visited = [False] * (len(self.graph))

        # Create a queue for BFS
        queue = []

        # Result Queue
        result = Queue()

        # Mark the source node as
        # visited and enqueue it
        queue.append(s)
        visited[s] = True

        while queue:
            # Dequeue a vertex from
            # queue and print it
            s = queue.pop(0)
            result.enqueue(s)
            # print(str(s) + " ")


            for i in self.graph[s]:
                if visited[i] == False:
                    queue.append(i)
                    visited[i] = True

        # result.printQueue()
        return result

        # Output data
    def output(self):
        for key, value in self.graph.iteritems():
            print("{} -> {}".format(key, value))

    def outputMatrix(self):
        print "\n\tMatrix"
        for raw in range(len(self.matrix)):
            str_raw = ""
            for col in self.matrix[raw]:
                str_raw += str(col.value) + " "
            print str_raw



