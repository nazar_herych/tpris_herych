# import random, math
import threading

outputdebug = False


def debug(msg):
    if outputdebug:
        print msg


class Node():
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None


class AVLTree():

    class myThread(threading.Thread):
        def __init__(self, threadID, data, action):
            threading.Thread.__init__(self)
            self.threadID = threadID
            self.data = data
            self.action = action

        def run(self):
            self.action(self.data)


    def __addNode(self, key):
        tree = self.node

        newnode = Node(key)

        if tree == None:
            self.node = newnode
            self.node.left = AVLTree()
            self.node.right = AVLTree()
            debug("Inserted key [" + str(key) + "]")

        elif key < tree.key:
            self.node.left.insert(key)

        elif key > tree.key:
            self.node.right.insert(key)

        else:
            debug("Key [" + str(key) + "] already in tree.")


    def insert(self, key):
        # print("Create separete Thread for insertion")

        thread1 = self.myThread(1, key, self.__addNode)

        # Start new Threads
        thread1.start()


    def __init__(self, *args):
        self.node = None
        self.height = -1

        if len(args) == 1:
            for i in args[0]:
                self.insert(i)

    def height(self):
        if self.node:
            return self.node.height
        else:
            return 0

    def is_leaf(self):
        return (self.height == 0)


    def delete(self, key):
        if self.node != None:
            if self.node.key == key:
                debug("Deleting ... " + str(key))
                if self.node.left.node == None and self.node.right.node == None:
                    self.node = None  # leaves can be killed at will
                # if only one subtree, take that
                elif self.node.left.node == None:
                    self.node = self.node.right.node
                elif self.node.right.node == None:
                    self.node = self.node.left.node

                # worst-case: both children present. Find logical successor
                else:
                    replacement = self.logical_successor(self.node)
                    if replacement != None:  # sanity check
                        debug("Found replacement for " + str(key) + " -> " + str(replacement.key))
                        self.node.key = replacement.key

                        # replaced. Now delete the key from right child
                        self.node.right.delete(replacement.key)

                return
            elif key < self.node.key:
                self.node.left.delete(key)
            elif key > self.node.key:
                self.node.right.delete(key)

        else:
            return

    def logical_successor(self, node):
        node = node.right.node
        if node != None:

            while node.left != None:
                debug("LS: traversing: " + str(node.key))
                if node.left.node == None:
                    return node
                else:
                    node = node.left.node
        return node


    def display(self, level=0, pref=''):
        if (self.node != None):
            print '-' * level * 2, pref, self.node.key, 'L' if self.is_leaf() else ' '
            if self.node.left != None:
                self.node.left.display(level + 1, '<')
            if self.node.left != None:
                self.node.right.display(level + 1, '>')


