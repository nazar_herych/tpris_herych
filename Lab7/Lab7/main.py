from TreeThread import AVLTree


def treeMenu():
    print("\tMenu:")
    print("1. Insert value To disorderly Tree(AVL): ")
    print("2. Delete from tree")
    print("3. Output Tree")
    print("0. Exit")

    tree = AVLTree()

    while True:
        choise = int(input("\n>Your choise: "))

        if choise == 1:
            value = raw_input("Enter new value to Tree:  ")

            tree.insert(value)
        if choise == 2:
            value = raw_input("Enter node you want delete:  ")

            tree.delete(value)
        if choise == 3:
            print("--- Output Tree ---")
            tree.display()

        if choise == 0:
            break



if __name__ == "__main__":

    treeMenu()
    pass

    a = AVLTree()
    print "----- Inserting -------"
    # inlist = [5, 2, 12, -4, 3, 21, 19, 25]
    inlist = [7, 5, 2, 6, 3, 4, 1, 8, 9, 0]
    for i in inlist:
        a.insert(i)

    a.display()

    print "----- Deleting -------"
    a.delete(3)
    # a.delete(4)
    # a.delete(5)
    a.display()

    print
    print "Input            :", inlist
    print "deleting ...       ", 3
    print "deleting ...       ", 4
    print "Inorder traversal:", a.inorder_traverse()





