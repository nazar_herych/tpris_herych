from FirebaseManager import FirebaseManager
from Recursive import Series
import time

if __name__ == '__main__':
    print "Start"

    manager = FirebaseManager()

    series = Series(40, 1)
    f = lambda x: float(((3 * float(x)) - 2) / (3 * float(x)))

    series.setFunction(f)

    # Track time of iterative method
    iterative_start_time = time.clock()

    # Calculate
    iterative = series.iterativeMethod()

    # Stop Time
    iterative_time = time.clock() - iterative_start_time


    # Track time of recursive method
    recursive_start_time = time.clock()

    # Calculate
    recursive = series.recursiveMethod()

    # Stop Time
    recursive_time = time.clock() - recursive_start_time


    print("\n\t>ITERATIVE")
    print("Result = {}".format(iterative))
    print("Time of execution: {}".format(iterative_time))

    print("\n\t>RECURSIVE")
    print("Result = {}".format(recursive))
    print("Time of execution: {}".format(recursive_time))

    series_id = manager.addValue(series.getCount(), series.getStart(), u'Multiplication',
                     recursive_time, recursive, iterative_time, iterative)

    print "\nSaved with ID: {}".format(series_id)