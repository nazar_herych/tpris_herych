
class Series:
    def __init__(self, top=None, start=1):
        self.__top = top
        self.__start = start
        self.__Function = None

    def getCount(self):
        return self.__top

    def getStart(self):
        return self.__start

    def setFunction(self, func):
        self.__Function = func

    def func(self, x):
        return float(((3 * float(x)) - 2) / (3 * float(x)))

    def iterativeMethod(self):
        y = 1
        for i in range(self.__start, self.__top+1):
            if not self.__Function:
                raise Exception("Function is not definded!!!")
            else:
                y *= self.__Function(i)

        return y

    def recursiveMethod(self, n=None):
        y = 1

        if n == 1:
            return self.__Function(1)
        elif n is None:
            return self.recursiveMethod(self.__top)

        y *= self.__Function(n) * self.recursiveMethod(n-1)

        return y

