

import firebase_admin
from firebase_admin import credentials, firestore
import uuid

class FirebaseManager(object):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(FirebaseManager, cls).__new__(cls)
            cls.__cred = credentials.Certificate("./serviceAccountKey.json")
            firebase_admin.initialize_app(cls.__cred)

            cls.db = firestore.client()
        return cls.instance

    def addValue(self, top, start, operation, recurs_time, recurs, iter_time, iter):
        seriesUID = str(uuid.uuid4())
        series_ref = self.db.collection(u'Series').document(unicode(seriesUID))

        list = {}

        list[unicode("Top")] = unicode(top)
        list[unicode("Start")] = unicode(start)
        list[unicode("Operation")] = unicode(operation)
        list[unicode("Recursive")] = unicode(recurs)
        list[unicode("Recursive_time")] = unicode(recurs_time)
        list[unicode("Iterative")] = unicode(iter)
        list[unicode("Iterative_time")] = unicode(iter_time)

        series_ref.set(list)

        return seriesUID
