# from FirebaseManager import  FirebaseManager

from DoublyLinkedList import Node, LinkedList
from Tree import TreeNode

from random import *
import json
import requests

# manager = FirebaseManager()

def generateList(count):
    linkList = LinkedList(node=Node(data=0))
    counter = 0
    while counter != count:
        linkList.push(Node(data=randint(1, 100)))
        counter += 1

    return  linkList


def listMenu():
    print("\tMENU:")
    print("1. Add data to LIST")
    print("2. Generate data for LIST")
    print("3. Remove from List")
    print("4. Output LIST")
    print("5. Save to FIREBASE")
    print("6. Load from FIREBASE")
    print("0. Exit")

    linkList = None

    while True:
        data_structure = int(input("\nYour choise: "))

        if data_structure == 1:
            if linkList is None:
                data = str(raw_input("Type data for Start = "))
                linkList = LinkedList(Node(data=data))
            else:
                data = str(raw_input("Type data for Node = "))
                linkList.push(Node(data=data))

        elif data_structure == 2:
            size = int(raw_input("Write size of generated list = "))
            print(size)
            linkList = generateList(size)
            print("GENERATE")
        elif data_structure == 3:
            index = int(raw_input("Type index of Node you want to remove"))
            linkList.remove(index=index)
            print("remove")
        elif data_structure == 4:
            print("LIST:")
            print(linkList)
        elif data_structure == 5:
            print("\t... saving to Firebase")
            jsObj = json.dumps(LinkedList.to_dict(linkList), ensure_ascii=False)
            print(jsObj)
            requests.post(url='http://localhost:8080/SaveList', json=jsObj)
            print("Saved!")
        elif data_structure == 6:
            print("\t... loading from Firebase")
            response = requests.get(url='http://localhost:8080/LoadList')
            print(response.content)
            linkList = LinkedList.from_dict(json.loads(response.content))
            print("LOADED")
        elif data_structure == 0:
            break
        else:
            print("Choose menu(0-6")


def treeMenu():
    print("\tMENU:")
    print("1. Add Tree-node: ")
    print("2. Remove the element from a tree")
    print("3. Print Tree")
    print("4. Save to FIREBASE")
    print("0. Exit")

    root = None

    while True:
        data_structure = int(input("\nYour choise: "))

        if data_structure == 1:
            if root is None:
                data = str(raw_input("Type data for ROOT = "))
                root = TreeNode(data)
            else:
                parent = str(raw_input("Type parent for node = "))
                data = str(raw_input("Type data for NEW node = "))
                root.searchForData(parent).addChild(TreeNode(data=data))
        elif data_structure == 2:
            data = str(raw_input("Type data node You want delete:  "))
            searchNode = root.searchForData(data)

            print("\tRemoving...")
            if searchNode is not None:
                searchNode.remove()

        elif data_structure == 3:
            print("output of TREE")
            print("root --->,()-->,()-->")
            print(root.strOfTree())
        elif data_structure == 4:
            print("\t... saving to Firebase")

            dict = TreeNode.to_dict(root)

            root_dict = {}
            root_dict[u'Root'] = root.data

            dict.update(root_dict)
            jsObj = json.dumps(dict, ensure_ascii=False)
            print(jsObj)
            requests.post(url='http://localhost:8080/SaveTree', json=jsObj)
            print("Saved!")




if __name__ == '__main__':
    print("CHOOSE DATA STRUCTURE: 1<DOUBLE_LINKED_LIST>  -  2<TREE>")
    data_structure = 0
    while True:
        data_structure = int(raw_input("\nDo your decision:  "))

        if data_structure == 1:
            listMenu()
            break
        else:
            treeMenu()
            break

