from random import *

class Node():
    def __init__(self, next_node=None, previous_node=None, data=None, index=None):
        self.next_node = next_node
        self.previous_node = previous_node
        self.data = data
        self.index = index


class LinkedList():
    element_Count = 0

    @staticmethod
    def to_dict(list):

        notEmpty = lambda x: x if x is not None else u'None'

        next_node = list.first_node
        dict = {}
        while next_node:
            inner_dict = {}
            next = next_node.next_node; previous = next_node.previous_node
            inner_dict[u'data'] = notEmpty(next_node.data)
            if next:
                inner_dict[u'next'] = notEmpty(next_node.next_node.data)
            if previous:
                inner_dict[u'previous'] = notEmpty(next_node.previous_node.data)
            dict[next_node.index] = inner_dict
            next_node = next_node.next_node
        return dict

    @staticmethod
    def from_dict(dict):
        print(dict[u'0'])
        list = LinkedList(Node(data=dict[u'0'][u'data']))


        for key, value in sorted(dict.items()):

            if key == u'0':
                continue

            keylist = value.keys()
            keylist.sort()

            list.push(Node(data=dict[key][u'data']))
        return list

    def count(self):
        return self.element_Count

    def __init__(self, node):
        assert isinstance(node, Node)
        self.first_node = node
        self.last_node = node

        node.index = self.count()
        self.element_Count += 1

    def push(self, node):

        node.next_node = self.first_node
        node.previous_node = None
        node.index = self.count()
        self.first_node.previous_node = node
        self.first_node = node

        self.element_Count += 1


    def pop(self):
        '''Pops the last node out of the list'''
        old_last_node = self.last_node
        to_be_last = self.last_node.previous_node
        to_be_last.next_node = None
        old_last_node.previous_node = None

        # Set the last node to the "to_be_last"
        self.previous_node = to_be_last

        return old_last_node

    def remove(self, node):
        next_node = node.next_node
        previous_node = node.previous_node

        previous_node.next_node = next_node
        next_node.previous_node = previous_node

        node.next_node = node.previous_node = None
        self.element_Count -= 1

        return node

    def remove(self, index):
        next_node = self.first_node
        while next_node:
            if next_node.index == index:
                next = next_node.next_node
                previous = next_node.previous_node

                # Index
                prev_ind = previous
                while prev_ind is not None:
                    prev_ind.index -= 1
                    prev_ind = prev_ind.previous_node

                self.element_Count -= 1


                previous.next_node = next
                next.previous_node = previous

                #Clean
                next_node.next_node = next_node.previous_node = None

                self.element_Count -= 1
                return next_node

            next_node = next_node.next_node

    def __str__(self):
        next_node = self.first_node
        s = ""
        while next_node:
            s += "--({})-->{}\n".format(next_node.index, next_node.data)
            next_node = next_node.next_node

        return s

        # return



#
# node1 = Node(data=1)
#
# linked_list = LinkedList(node1)
#
# for i in xrange(12):
#     if i == 5:
#         node5 = Node(data=5)
#         linked_list.push(node5)
#     else:
#         linked_list.push(Node(data=i))
#
# print linked_list
#
# print "popping"
# print linked_list.pop().data
#
# print "\n\n"
# print linked_list
#
#
# print "\n\n"
# linked_list.push(Node(data=10))
#
# print "\n\n"
# print linked_list
#
# linked_list.remove(7)
# linked_list.remove(4)
#
# print "\n\n"
# print linked_list