
class TreeNode:
    def __init__(self, data):
        self.parent = None
        self.children = []
        self.data = data


    @staticmethod
    def to_dict(node):
        dict = {}
        child_dict = {}
        for index, child in enumerate(node.children):
            child_dict[index] = child.data
            dict.update(TreeNode.to_dict(child))
        dict[node.data] = child_dict

        return dict


    # Data {GET SET}
    def setData(self, data):
        self.data = data

    def getData(self):
        return self.data


    # Parent {GET SET}
    def setParent(self, parent):
        self.parent = parent

    def getParent(self):
        return self.parent


    # Add child
    def addChild(self, child):
        self.children.append(child)
        child.parent = self

    def removeChild(self, child):
        self.children.remove(child)

    def getChildren(self):
        return self.children


    # Remove NODE
    def remove(self):
        children = self.children
        parent = self.parent

        # Node child  <inherited>  Parent child
        for child in children:
            parent.addChild(child)
        parent.removeChild(self)


    def searchForData(self, data):
        if self.data == data:
            return self
        else:
            child = None
            for child in self.children:
                child = child.searchForData(data)
            return child

    # STRING performing
    def strOfTree(self):
        childStr = ""
        if len(self.children) > 0:
            childStr = "["
            for child in self.children:
                index = self.children.index(child)
                if (index == 1 or index == len(self.children)-1) and len(self.children) != 1:
                    childStr += ", "
                childStr += child.strOfTree()
            childStr += "]"

        return " {} {}".format(str(self.data), childStr)

    def __str__(self):
        return str(self.data)


# Use the insert method to add nodes
root = TreeNode(12)

left = TreeNode(3)
right = TreeNode(8)

root.addChild(left)
root.addChild(right)

left.addChild(TreeNode(5))
right.addChild(TreeNode(6))
right.addChild(TreeNode(0))


# right.remove()

print root.strOfTree()